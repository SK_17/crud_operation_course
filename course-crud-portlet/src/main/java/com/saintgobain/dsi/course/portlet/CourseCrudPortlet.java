package com.saintgobain.dsi.course.portlet;

import com.saintgobain.dsi.course.constants.CourseCrudPortletKeys;
import com.saintgobain.dsi.course.model.Course;
import com.saintgobain.dsi.course.util.CourseUtil;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author liferay
 */
/**
 * @author S7520277
 *
 */
/**
 * @author S7520277
 *
 */
/**
 * @author S7520277
 *
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=course-crud-portlet Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CourseCrudPortletKeys.CourseCrud,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CourseCrudPortlet extends MVCPortlet {
 

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
			
		String viewType = renderRequest.getParameter("viewType");      

		
		if(Validator.isNotNull(viewType) && viewType.equals("createCourse")){
			
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/createCourse.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
		   }
		   else if(Validator.isNotNull(viewType) && viewType.equals("listCourse")){
			
			List<Course>courseList = CourseUtil.getCourseList();
			renderRequest.setAttribute("courseList", courseList);
			System.out.println(courseList);
			PortletRequestDispatcher dispatcherlistCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/listCourse.jsp");
			dispatcherlistCourse.include(renderRequest,renderResponse);
		   }
		   
		   else if(Validator.isNotNull(viewType)&&viewType.equals("editCourse")){
			
			String oldCourseName = renderRequest.getParameter("courseName");
			Course editCourse= CourseUtil.getCourseByName(oldCourseName);
			renderRequest.setAttribute("editCourse", editCourse);
			System.out.println();
			PortletRequestDispatcher dispatchereditCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/editCourse.jsp");
			dispatchereditCourse.include(renderRequest,renderResponse);
		   }
		   
		   else if(Validator.isNotNull(viewType)&&viewType.equals("deleteCourse")){
			
			String CourseName = renderRequest.getParameter("courseName");
			boolean course= CourseUtil.deleteCourseByName(CourseName);
			List<Course>courseList = CourseUtil.getCourseList();
			renderRequest.setAttribute("courseList", courseList);
			System.out.println(courseList);
			PortletRequestDispatcher dispatchereditCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/listCourse.jsp");
			dispatchereditCourse.include(renderRequest,renderResponse);
			}
		   else {
			PortletRequestDispatcher dispatchereditCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/view.jsp");
			dispatchereditCourse.include(renderRequest,renderResponse);
		}

	}
	
	// To add Course to the list
	@ProcessAction(name ="addCourse")
	public void addCourse(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws IOException, PortletException {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
		
		Course course = new Course();
		String CourseName = GetterUtil.get(ParamUtil.getString(actionRequest, "course"),StringPool.BLANK);
		String StartDate = GetterUtil.get(ParamUtil.getString(actionRequest, "startDate"),StringPool.BLANK);
		String EndDate = GetterUtil.get(ParamUtil.getString(actionRequest, "endDate"),StringPool.BLANK);
	
		
		Course existingCourse = CourseUtil.getCourseByName(CourseName);
		
		if(existingCourse != null ){
		    throw new PortletException("Course name already exists.");
		} else {
		    // proceed with add the course
		    LocalDate startDate = LocalDate.parse(StartDate, formatter);
		    LocalDate endDate = LocalDate.parse(EndDate, formatter);
		    course.setCourseName(CourseName);
		    course.setStartDate(startDate);
		    course.setEndDate(endDate);
		    CourseUtil.addCourse(course);
		    actionResponse.setRenderParameter("viewType", "listCourse");
		}

	
	}
	
	// to edit course to list
	@ProcessAction(name ="editCourse")
	public void editCourse(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws IOException, PortletException {
		    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
		    Course course = new Course();
		    String oldCourseName = actionRequest.getParameter("updateCourseName");
		    String CourseName = actionRequest.getParameter("courseName");
		    String StartDate = actionRequest.getParameter("startDate");
		    String EndDate = actionRequest.getParameter("endDate");
		    
		    try{
		    	LocalDate startDate = LocalDate.parse(StartDate, formatter);
				 LocalDate endDate = LocalDate.parse(EndDate, formatter);
				 course.setCourseName(CourseName);
				 course.setStartDate(startDate);
				 course.setEndDate(endDate);
				 CourseUtil.updateCourseByName(oldCourseName, course);
				
				
				 actionResponse.setRenderParameter("viewType",
						    "listCourse");
		    }catch (Exception e) {
				e.printStackTrace();
			}
		    
		    
		   
		    }
	
	private Log _log =LogFactoryUtil.getLog(CourseCrudPortlet.class);
}
