package com.saintgobain.dsi.course.model;

import java.time.LocalDate;

public class Course {
	
	private String CourseName;
	private LocalDate StartDate;
	private LocalDate EndDate;
	
	
	public Course() {
	}
    
	public Course(String courseName){
		CourseName = courseName;
	}
	
	public Course(String courseName, LocalDate startDate, LocalDate endDate) {
		super();
		CourseName = courseName;
		StartDate = startDate;
		EndDate = endDate;
	}


	public String getCourseName() {
		return CourseName;
	}


	public void setCourseName(String courseName) {
		CourseName = courseName;
	}


	public LocalDate getStartDate() {
		return StartDate;
	}


	public void setStartDate(LocalDate startDate) {
		StartDate = startDate;
	}


	public LocalDate getEndDate() {
		return EndDate;
	}


	public void setEndDate(LocalDate endDate) {
		EndDate = endDate;
	}


	@Override
	public String toString() {
		return "Course [CourseName=" + CourseName + ", StartDate=" + StartDate + ", EndDate=" + EndDate + "]";
	}

	
	
	
	

}
