package com.saintgobain.dsi.course.util;

import com.liferay.portal.kernel.util.Validator;
import com.saintgobain.dsi.course.model.Course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class CourseUtil {
	
	private static List<Course> courseList;

	
	/**
	 *  to add course to List
	 * @param course
	 */
	public static void addCourse(Course course){
		if(Validator.isNull(courseList))
			courseList = new ArrayList<>();
		
		courseList.add(course);
		
	}
	
	
	
	/**
	 * to get single course deatil by their coursename
	 * @param courseName
	 * @return
	 */
	public static Course getCourseByName(String courseName){
		List<Course> courseList = CourseUtil.getCourseList();
		Optional<Course> getCourse = courseList.stream()
				.filter(course -> course.getCourseName().equals(courseName))
				.findFirst();
            if(getCourse.isPresent()){
            	Course course = getCourse.get();
                course.setCourseName(course.getCourseName());
                course.setStartDate(course.getStartDate());
                course.setEndDate(course.getEndDate());
               
                return course;
            }
        
		return null;
        
	}
	
	
	
	/**
	 * to update the course in list
	 * @param oldCourseName
	 * @param newCourse
	 * @return
	 */
	public static boolean updateCourseByName(String oldCourseName, Course newCourse) {
	    List<Course> courseList = CourseUtil.getCourseList();
	    OptionalInt index = IntStream.range(0, courseList.size())
	        .filter(i -> courseList.get(i).getCourseName().equals(oldCourseName))
	        .findFirst();
	    if (index.isPresent()) {
	        int i = index.getAsInt();
	        courseList.set(i, newCourse);
	        return true;
	    } else {
	        return false;
	    }
	}
	
	
	 
	/**
	 * 
	 * <p>to delete a course from list by their course name
	 * @param courseName
	 * @return
	 */
	public static boolean deleteCourseByName(String courseName) {
		OptionalInt index = IntStream.range(0, courseList.size())
				.filter( i -> courseList.get(i).getCourseName().equals(courseName))
				.findFirst();
	        if (index.isPresent()) {
	            courseList.remove(index.getAsInt());
	            return true;
	        }else{
	        return false;
	    }
	}
	


   
	/**
	 * to get the arraylist
	 * @return
	 */
	public static List<Course> getCourseList(){
		
		if(Validator.isNull(courseList))
			return new ArrayList<>();
		
		return courseList;
	}

	
	}
	
	

