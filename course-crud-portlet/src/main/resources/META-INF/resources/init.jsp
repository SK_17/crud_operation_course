<%@page import="javax.portlet.RenderRequest"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.saintgobain.dsi.course.model.Course"%>
<%@page import="com.saintgobain.dsi.course.util.CourseUtil"%>

<%@page import="java.util.List"%>


<liferay-theme:defineObjects />


<portlet:defineObjects />
<portlet:renderURL  var="createCourseViewURL">
<portlet:param name="viewType" value="createCourse"/>
</portlet:renderURL>

<portlet:renderURL  var="listCourseViewURL">
<portlet:param name="viewType" value="listCourse"/>
</portlet:renderURL>

<portlet:actionURL  name = "addCourse"var="addCourseActionURL"></portlet:actionURL>


>
<% 
PortletURL createCourseURL= renderResponse.createRenderURL();
createCourseURL.setParameter("createCourseViewURL", "createCourse");
%>
<% 
PortletURL listCourseURL = renderResponse.createRenderURL();
listCourseURL.setParameter("listCourseViewURL", "listCourse");
%>
<% 
PortletURL editCourseURL= renderResponse.createRenderURL();
createCourseURL.setParameter("editCourseViewURL", "editCourse");
%>
<% 
PortletURL homeVieweURL = renderResponse.createRenderURL();
homeVieweURL.setParameter("mvcPath", "/META-INF/resources/view.jsp");
%>

