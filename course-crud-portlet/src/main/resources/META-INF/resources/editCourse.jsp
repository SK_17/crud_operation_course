<%@page import="com.saintgobain.dsi.course.util.CourseUtil"%>
<%@page import="com.saintgobain.dsi.course.model.Course"%>

<%@ include file="/init.jsp" %>
<a href="<%= homeVieweURL.toString()%>">Home </a>
<%
Course  editCourse =(Course) renderRequest.getAttribute("editCourse");
if( editCourse!=null){
		
%>
<portlet:actionURL  name = "editCourse"var="editCourseactionURL">
<portlet:param name="courseName" value=""/>
</portlet:actionURL>

<aui:form method="POST" action="<%= editCourseactionURL%>">
<aui:input name="updateCourseName" type="hidden" value="<%=editCourse.getCourseName()%>" /> 
<aui:input name="startDate" type="date" value="<%=editCourse.getStartDate()%>" > <aui:validator name="required" /></aui:input>
<aui:input name="endDate" type="date" value="<%=editCourse.getEndDate()%>" > <aui:validator name="required" /></aui:input>
<aui:input name="courseName" type="text" value="<%=editCourse.getCourseName()%>" > <aui:validator name="required" /></aui:input>
<aui:button type="submit" value="submit" />
</aui:form>
  
</script>
<%}
%>