
<%@page import="com.saintgobain.dsi.course.util.CourseUtil"%>
<%@page import="com.saintgobain.dsi.course.model.Course"%>
<%@ include file="/init.jsp" %>
<h1> Course Details</h1>
<a href="<%= createCourseViewURL%>">create course</a>

<%
List<Course> courseList = (List<Course>) renderRequest.getAttribute("courseList");

if(Validator.isNotNull(courseList)){
	
%>
<table class="table table-light">
			
				<tr>
				    
					<th scope="col">Course</th>
					<th scope="col">Start Date</th>
					<th scope="col">End date</th>
					<th scope="col"></th>
					<th scope="col"></th>
					
				</tr>
		<c:forEach items="${courseList}" var="course">  
    
        <portlet:renderURL var="editCourseViewURL">
             <portlet:param name="viewType" value="editCourse"/>
            <portlet:param name="courseName" value="${course.courseName}"/>
        </portlet:renderURL>
			 <portlet:renderURL var="deleteCourseViewURL">
             <portlet:param name="viewType" value="deleteCourse"/>
            <portlet:param name="courseName" value="${course.courseName}"/>
        </portlet:renderURL>
					<tr>
				    
					<th scope="col">${course.getCourseName()}</th>
					<th scope="col">${course.getStartDate()}</th>
					<th scope="col">${course.getEndDate()}</th>
					<th><a class="btn btn-sm  btn-danger"  href="<%=editCourseViewURL%>">Edit</a></td>
					<th><a class="btn btn-sm  btn-warning"  href="<%=deleteCourseViewURL%>">Delete</a></td>
				</tr>		
			 </c:forEach>
			</table>
			<%} %>
			
			